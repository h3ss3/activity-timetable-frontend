# Activity Timetable Frontend

Keep a trace of your work and don't be afraid to monthly fill your timesheet with this simple AngularJs application frontend. To use with its necessary backend part [activity-timetable-backend](https://gitlab.com/h3ss3/activity-timetable-backend)

## Overview

![Overview screenshot](resources/overview.jpg "Overview of the application")

Manage tasks you do at work along the day and have a week by week summary and a quick sumup by project for the selected month. There is no need to reload page to refresh your updates.

When editing a task, no need to click any button to save your project update or your time update, it's automatically done but when you finish to edit your description, click on save button to save the description update.

## Installation

### Step 1

```shell
# Install node dependencies
# (dependencies are in package.json but not found how to specify a prefix, so for now, install them one by one...)
npm install -g uglifycss --prefix="vendor/node_modules"
npm install -g uglify-js --prefix="vendor/node_modules"
npm install -g less --prefix="vendor/node_modules"
npm install -g bower --prefix="vendor/node_modules"

# Install bower dependencies
bower install
```

### Step 2: build your parameters file
You have a dist parameters file `app/js/config/parameters.js.dist`, copy it to `app/js/config/parameters.js` and set values :
 - **BASE_API_URL :** complete base URI of your backend API activity-timetable-backend (e.g *http://timetable.local/app_dev.php/api/v1*)
 - **ACTIVITY_TIMETABLE_BEGINNING :** used as the day of the first task (e.g *new Date(2014, 8, 1, 0, 0, 0)* for August 1st 2014, tasks before this date won't be considered)

#### Corresponding example:
```
// app/js/config/parameters.js

var parameters = {
    'BASE_API_URL': 'http://timetable.local/app_dev.php/api/v1',
    'ACTIVITY_TIMETABLE_BEGINNING': new Date(2014, 8, 1, 0, 0, 0)
}
```

## Future evolutions

- For now, there is nothing to manage projects, you must add projects directly in the database on backend part.
- It would be great to have hierarchical projects (projects, sub projects, etc.) to have a better trace of your work
- Prettier design
