app.directive('inputInteger', InputInteger);

function InputInteger() {
    return {
        scope: {
            ngModel: '=',
            ngChange: '&',
            increment: '&',
            decrement: '&'
        },
        template:   '<div class="input-group spinner">' +
                        '<input type="text" class="form-control" ng-model="ngModel" ng-change="ngChange()" />' +
                        '<div class="input-group-btn-vertical">' +
                            '<button class="btn btn-default" ng-click="increment()"><i class="fa fa-caret-up"></i></button>' +
                            '<button class="btn btn-default" ng-click="decrement()"><i class="fa fa-caret-down"></i></button>' +
                        '</div>' +
                    '</div>'
    };
};
