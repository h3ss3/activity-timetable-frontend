app.controller('newTaskCtrl', NewTaskCtrl);

/*@ngInject*/
function NewTaskCtrl($scope, $rootScope, projectApi, taskApi) {

//private :

    var _this      = this;
    $scope.newTask = {};

    projectApi
        .cget()
        .success(function(data, status, headers, config) {
            $scope.projects = data;
        }).error(function(data, status, headers, config) {
            _this.displayError(data, status, headers, config);
        });

//public :

    this.resetTask = function() {
        $scope.newTask = {
            day: new Date().format('Y-m-d'),
            project: null,
            duration: 1
        }
    }

    $scope.send = function() {
        $rootScope.$emit('PRE_CREATE_TASK')
        taskApi
            .post($scope.newTask)
            .success(function(data, status, headers, config) {
                $rootScope.$emit('POST_CREATE_TASK')
                _this.resetTask();
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    $scope.incrementDuration = function() {
        $scope.newTask.duration++;
    }

    $scope.decrementDuration = function() {
        if ( $scope.newTask.duration > 1 )
            $scope.newTask.duration--;
    }

    $rootScope.$on('CHANGE_MONTH', function() {
        // var month = $rootScope.selectedMonth;
        // $scope.newTask.day = month.year+"-"+month.month+"-01"
    });

    this.resetTask();
}
