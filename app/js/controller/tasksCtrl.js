app.controller('tasksCtrl', TasksCtrl);

/*@ngInject*/
function TasksCtrl($scope, $rootScope, $timeout, taskApi, projectApi, calculationApi) {

//private :

    var _this                     = this;
    this.changeDurationDelayed    = {};
    this.changeDescriptionDelayed = {};
    $scope.weeksOfWork            = {};
    $scope.projects               = {};

    $scope.delete = function(taskId) {
        taskApi
            .delete(taskId)
            .success(function(data, status, headers, config) {
                $rootScope.$emit('POST_DELETE_TASK')
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    $scope.changeProject = function(taskId) {
        var task = _this.retrieveOneTask(taskId);
        taskApi
            .patch(taskId, {project: task.project.id})
            .success(function(data, status, headers, config) {
                $rootScope.$emit('POST_PATCH_TASK')
            })
            .error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
                $rootScope.$emit('POST_PATCH_TASK_FAILURE')
            })
    }

    $scope.changeDuration = function(taskId, duration) {
        taskApi
            .patch(taskId, {duration: duration})
            .success(function(data, status, headers, config) {
                $rootScope.$emit('POST_PATCH_TASK')
            })
            .error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
                $rootScope.$emit('POST_PATCH_TASK_FAILURE')
            })
    }

    $scope.changeDescription = function(taskId) {
        var task = _this.retrieveOneTask(taskId);

        taskApi
            .patch(taskId, {description: task.description})
            .success(function(data, status, headers, config) {
                $rootScope.$emit('POST_PATCH_TASK')
            })
            .error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
                $rootScope.$emit('POST_PATCH_TASK_FAILURE')
            })
    }

    $scope.incrementDuration = function(taskId) {
        var task = _this.retrieveOneTask(taskId);
        task.duration++;

        $scope.switchChangeDurationDelayed(taskId, task.duration);
    }

    $scope.decrementDuration = function(taskId) {
        var task = _this.retrieveOneTask(taskId);
        if ( task.duration > 1 )
            task.duration--;

        $scope.switchChangeDurationDelayed(taskId, task.duration);
    }

    $scope.switchChangeDurationDelayed = function(taskId, duration) {
        $timeout.cancel(_this.changeDurationDelayed[taskId]);

        _this.changeDurationDelayed[taskId] = $timeout(function() { $scope.changeDuration(taskId, duration); }, 1000);
    }

    this.retrieveProjects = function() {
        projectApi
            .cget()
            .success(function(data, status, headers, config) {
                var projects = [];
                for(index in data) {
                    var project = data[index];
                    projects[project.id] = project;
                }
                $scope.projects = projects;
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    this.retrieveOneTask = function(taskId) {
        for(week in $scope.weeksOfWork) {
            for(day in $scope.weeksOfWork[week]["day_of_works"]) {
                for (taskIndex in $scope.weeksOfWork[week]["day_of_works"][day]["tasks"]) {
                    var task = $scope.weeksOfWork[week]["day_of_works"][day]["tasks"][taskIndex];
                    if (task.id==taskId)
                        return task;
                }
            }
        }
        return null;
    }

    this.retrieveWeeksOfWork = function() {
        calculationApi
            .cget($rootScope.selectedMonth.year(), $rootScope.selectedMonth.month())
            .success(function(data, status, headers, config) {
                $scope.weeksOfWork = data;
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    this.displayError = function(data, status, headers, config) {
        console.log('error ');
        console.log(status);
        console.log(data);
        console.log(headers);
        console.log(config);
    }

    this.retrieveProjects();
    this.retrieveWeeksOfWork();

    $rootScope.$on('CHANGE_MONTH', function() {
        _this.retrieveWeeksOfWork();
    });

    $rootScope.$on('POST_DELETE_TASK', function() {
        _this.retrieveWeeksOfWork();
    });

    $rootScope.$on('POST_CREATE_TASK', function() {
        _this.retrieveWeeksOfWork();
    });

    $rootScope.$on('POST_PATCH_TASK', function() {
        _this.retrieveWeeksOfWork();
    });

    $rootScope.$on('POST_PATCH_TASK_FAILURE', function() {
        _this.retrieveWeeksOfWork();
    });

//public :


}
