app.controller('monthSelectorCtrl', MonthSelectorCtrl);

/*@ngInject*/
function MonthSelectorCtrl($scope, $rootScope, ACTIVITY_TIMETABLE_BEGINNING) {
//private :

    $scope.months = [];
    $rootScope.selectedMonth = null;

    this.calculateMonths = function() {
        var now     = new Date();
        var current = ACTIVITY_TIMETABLE_BEGINNING.clone().firstDayOfMonth();
        while (current < now) {
            $scope.months.push(current.clone());
            current.firstDayOfNextMonth();
        }
    }

    this.resetSelectedMonth = function() {
        $rootScope.selectedMonth = $scope.months[$scope.months.length-1];
    }

    $scope.changeSelectedMonth = function() {
        var iYear  = parseInt($scope.selectedMonth.year());
        var iMonth = parseInt($scope.selectedMonth.month());
        for(month in $scope.months) {
            var oMonth = $scope.months[month];
            if (oMonth.year()==iYear && oMonth.month()==iMonth) {
                $rootScope.selectedMonth = oMonth;
            }
        }
        $rootScope.$emit('CHANGE_MONTH')
    }

    this.calculateMonths();
    this.resetSelectedMonth();

//public :

}
