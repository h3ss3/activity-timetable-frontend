app.controller('sumupCtrl', SumupCtrl);

/*@ngInject*/
function SumupCtrl($scope, $rootScope, statsCalculator, projectApi, taskApi) {

//private :

    var _this       = this;
    var projects    = {};
    $scope.projects = [];

    this.retrieveProjects = function() {
        projectApi
            .cget()
            .success(function(data, status, headers, config) {
                var projects = [];
                for(index in data) {
                    var project = data[index];
                    projects[project.id] = project;
                }
                _this.projects = projects;
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    this.retrieveTasks = function() {
        var date  = $rootScope.selectedMonth.clone().firstDayOfMonth();
        var start = date.format('Y-m-d');
        var end   = date.lastDayOfMonth().format('Y-m-d');
        taskApi
            .cget(start, end)
            .success(function(data, status, headers, config) {
                $scope.totalDuration = statsCalculator.calculateTotalDuration(data);
                var durations = statsCalculator.calculateDurations(data);
                var projects = [];
                for(projectId in durations) {
                    projects.push({
                        id       : projectId,
                        name     : _this.projects[projectId].name,
                        duration : durations[projectId]
                    });
                }
                $scope.projects = projects;
            }).error(function(data, status, headers, config) {
                _this.displayError(data, status, headers, config);
            });
    }

    this.displayError = function(data, status, headers, config) {
        console.log('error ');
        console.log(status);
        console.log(data);
        console.log(headers);
        console.log(config);
    }

    this.retrieveProjects();
    this.retrieveTasks();

    $rootScope.$on('POST_CREATE_TASK', function() {
        _this.retrieveTasks();
    });

    $rootScope.$on('CHANGE_MONTH', function() {
        _this.retrieveTasks();
    });

    $rootScope.$on('POST_DELETE_TASK', function() {
        _this.retrieveTasks();
    });

    $rootScope.$on('POST_PATCH_TASK', function() {
        _this.retrieveTasks();
    });

//public :


}
