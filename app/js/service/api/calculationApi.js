app.service('calculationApi', CalculationApi);

/*@ngInject*/
function CalculationApi($http, BASE_API_URL) {
    
    this.cget = function(year, month) {
        var promise = $http.get(BASE_API_URL+'/calculations/weeks.json?year='+year+'&month='+month);

        return promise;
    }
}
