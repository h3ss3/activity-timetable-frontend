app.service('taskApi', TaskApi);

/*@ngInject*/
function TaskApi($http, BASE_API_URL) {
    
    this.cget = function(dayStart, dayEnd) {
        var promise = $http.get(BASE_API_URL+'/tasks.json?dayStart='+dayStart+'&dayEnd='+dayEnd);

        return promise;
    }

    this.post = function(data) {
        var promise = $http.post(BASE_API_URL+'/tasks.json', data);

        return promise;
    }

    this.patch = function(taskId, data) {
        var promise = $http.patch(BASE_API_URL+'/tasks/'+taskId+'.json', data);

        return promise;
    }

    this.delete = function(taskId) {
        var promise = $http.delete(BASE_API_URL+'/tasks/'+taskId+'.json');

        return promise;
    }
}
