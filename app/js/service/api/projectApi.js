app.service('projectApi', ProjectApi);

/*@ngInject*/
function ProjectApi($http, BASE_API_URL) {
    this.cget = function() {
        var promise = $http.get(BASE_API_URL+'/projects.json');

        return promise;
    }
}
