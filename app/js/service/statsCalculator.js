app.service('statsCalculator', StatsCalculator);

/*@ngInject*/
function StatsCalculator() {

    this.calculateDurations = function(tasks) {
        var projectDurations = [];
        for(index in tasks) {
            var task = tasks[index];
            if ( typeof projectDurations[task.project.id] === 'undefined' )
                projectDurations[task.project.id] = 0;
            projectDurations[task.project.id] += parseInt(task.duration);
        }

        return projectDurations;
    }

    this.calculateTotalDuration = function(tasks) {
        var totalDuration = 0;
        for(index in tasks) {
            var task = tasks[index];
            totalDuration += parseInt(task.duration);
        }

        return totalDuration;
    }
}
