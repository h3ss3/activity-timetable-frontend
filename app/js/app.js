var app = angular.module('ActivityTimetableFrontend', ['720kb.datepicker']);

if ( typeof parameters === 'undefined' ) {
    console.log("missing all parameters : app/js/config/parameters.js must exist "+
        "and must contain a definition of array parameters with same keys as "+
        "in app/js/config/parameters.js.dist")
}

var missingParameters = [];

for(key in parametersDist) {
    if ( typeof parameters[key] === 'undefined' )
        missingParameters.push(key);
}

if ( missingParameters.length > 0 ) {
    console.log("missing parameters ("+missingParameters+") in app/js/config/parameters.js : "+
        "compare with app/js/config/parameters.js.dist")
}

for(key in parameters)
    app.value(key, parameters[key]);
